# Frontend Exercise

The purpose of this project is to show my experience and knowledge as a Frontend.

I've created a responsive page with a clean style, using Flexbox to structure the layout and I've chosen blue as the main color because it is a neutral color. I used icons from Font Awesome to illustrate each part of the form.

Inside the folder __dist__ is the final result.


## Screenshots

Desktop version

![Desktop Screenshot](screenshots/desktop.jpg)

Mobile version

![Mobile Screenshot](screenshots/mobile.jpg)

Mobile version - Menu

![Mobile Menu Opened Screenshot](screenshots/mobile-menu.jpg)

## Installing and running

1. Clone this repository

`git clone ...`

2. Change to the directory

`cd vericatch front test`

3. Install the depencies

`npm install`

4. Run server

`gulp serve`


## Built With

- Gulp
- Sass
- Npm